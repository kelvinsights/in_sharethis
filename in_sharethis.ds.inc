<?php
/**
 * @file
 * in_sharethis.ds.inc
 */

/**
 * Implements hook_ds_custom_fields_info().
 */
function in_sharethis_ds_custom_fields_info() {
  $export = array();

  $ds_field = new stdClass();
  $ds_field->api_version = 1;
  $ds_field->field = 'sharethis_hcount';
  $ds_field->label = 'Example: Sharethis (hcount)';
  $ds_field->field_type = 5;
  $ds_field->entities = array(
    'node' => 'node',
  );
  $ds_field->ui_limit = '*|revision';
  $ds_field->properties = array(
    'code' => array(
      'value' => '<?
$url = url(drupal_get_path_alias(\'node/\'.$entity->nid), array(\'absolute\' => TRUE));
$title = $entity->title;
?>

<span class=\'st_fblike_hcount\' st_title=\'<?php print $title; ?>\' st_url=\'<?php print $url; ?>\' ></span>
<span class=\'st_twitter_hcount\' st_title=\'<?php print $title; ?>\' st_url=\'<?php print $url; ?>\'></span>
<span class=\'st_plusone_hcount\' st_title=\'<?php print $title; ?>\' st_url=\'<?php print $url; ?>\'></span>
<span class=\'st_linkedin_hcount\' st_title=\'<?php print $title; ?>\' st_url=\'<?php print $url; ?>\'></span>
<span class=\'st_pinterest_hcount\' displayText=\'Sina\' st_title=\'<?php print $title; ?>\' st_url=\'<?php print $url; ?>\'></span>
<span class=\'st_sina_hcount\' displayText=\'Weibo\' st_title=\'<?php print $title; ?>\' st_url=\'<?php print $url; ?>\'></span>
<span class=\'st_email_hcount\' st_title=\'<?php print $title; ?>\' st_url=\'<?php print $url; ?>\'></span>
<span class=\'st_sharethis_hcount\' st_title=\'<?php print $title; ?>\' st_url=\'<?php print $url; ?>\'></span>',
      'format' => 'ds_code',
    ),
    'use_token' => 1,
  );
  $export['sharethis_hcount'] = $ds_field;

  $ds_field = new stdClass();
  $ds_field->api_version = 1;
  $ds_field->field = 'sharethis_vcount';
  $ds_field->label = 'Example: Sharethis (vcount)';
  $ds_field->field_type = 5;
  $ds_field->entities = array(
    'node' => 'node',
  );
  $ds_field->ui_limit = '*|revision';
  $ds_field->properties = array(
    'code' => array(
      'value' => '<?
$url = url(drupal_get_path_alias(\'node/\'.$entity->nid), array(\'absolute\' => TRUE));
$title = $entity->title;
?>

<span class=\'st_fblike_vcount\' st_title=\'<?php print $title; ?>\' st_url=\'<?php print $url; ?>\' ></span>
<span class=\'st_twitter_vcount\' st_title=\'<?php print $title; ?>\' st_url=\'<?php print $url; ?>\'></span>
<span class=\'st_plusone_vcount\' st_title=\'<?php print $title; ?>\' st_url=\'<?php print $url; ?>\'></span>
<span class=\'st_linkedin_vcount\' st_title=\'<?php print $title; ?>\' st_url=\'<?php print $url; ?>\'></span>
<span class=\'st_pinterest_vcount\' st_title=\'<?php print $title; ?>\' st_url=\'<?php print $url; ?>\'></span>
<span class=\'st_sina_vcount\' displayText=\'Weibo\' st_title=\'<?php print $title; ?>\' st_url=\'<?php print $url; ?>\'></span>
<span class=\'st_email_vcount\' st_title=\'<?php print $title; ?>\' st_url=\'<?php print $url; ?>\'></span>
<span class=\'st_sharethis_vcount\' st_title=\'<?php print $title; ?>\' st_url=\'<?php print $url; ?>\'></span>',
      'format' => 'ds_code',
    ),
    'use_token' => 0,
  );
  $export['sharethis_vcount'] = $ds_field;

  return $export;
}
